/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medallistaolimpico.ui;

import com.medallistaolimpico.bl.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ComMedallistaOlimpico {

    
    
    public static void main(String[] args) {

        Pais costarica = new Pais(1, "Costa Rica", "CR");
        Provincia cartago = new Provincia(costarica, 2, "Cartago", "Cart");
        Canton cartagoCanton = new Canton(cartago, 3, "Cartago", "CantCart");
        Distrito central = new Distrito(cartagoCanton, 4, "Central", "central");
        Direccion direccion = new Direccion(costarica, cartago, cartagoCanton, central, "Ivenz Aguilar", "linea1", "linea2");
        Atleta ejemplo = new Atleta(LocalDate.parse("2021-01-01"), direccion, "masculino", 1, "Ivenz", "Josue", "Aguilar", "1111", costarica, "correo@ejemplo.com", "1234");
        Medalla medallaOro = new Medalla(1, "Medalla de Oro", "ejemplo", "descripcion");
        String[] coordenadas = {"1", "1"};
        String[] coordenadas2 = {"2", "2"};
        String[] coordenadas3 = {"3", "3"};
        Actividad correr = new Actividad(1, "act1", "Correr", "ruta");
        Reto reto1 = new Reto(1, "Reto Ejemplo", "descripcion", coordenadas, correr, costarica, medallaOro, "123");
        Hito hito1 = new Hito(1, reto1, 1, "ruta", "descripcion", coordenadas2);
        Hito hito2 = new Hito(2, reto1, 2, "ruta", "descripcion", coordenadas3);
        ArrayList<Hito> hitos = new ArrayList();
        hitos.add(hito1);
        hitos.add(hito2);
        reto1.setHitos(hitos);
        MetodoPago tarjeta = new MetodoPago(1, "Nombre Ejemplo", 111, 01, 24);
        Transaccion transaccion = new Transaccion(1, tarjeta, ejemplo, LocalDate.now(), "exitoso", "colon", 40.3, "debito");

        ArrayList<Object> esto = new ArrayList<>();

        esto.add(costarica);
        esto.add(cartago);
        esto.add(cartagoCanton);
        esto.add(central);
        esto.add(direccion);
        esto.add(ejemplo);
        esto.add(medallaOro);
        esto.add(correr);
        esto.add(reto1);
        esto.add(tarjeta);
        esto.add(transaccion);
        
        for (Object some : esto) {
            
            String text = some.toString();
            System.out.println(text + "\n");
              
            }

        
    }

}
